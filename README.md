## Anonymized code for "Cryptanalysis of Curl-P and Other Attacks on the IOTA Cryptocurrency"

See `examples` for the original colliding bundles we found.

See `valueattack`, `collide`, and `template` for the code to create colliding bundles.

Make sure to set your `GOPATH` and check out this repo to `$GOPATH/src/gitlab.com/curl-paper/code`. For example, the following sets `GOPATH` to a directory named `go` inside your home directory and clones the repo there:

```
export GOPATH=$HOME/go
mkdir -p $GOPATH/src/gitlab.com/curl-paper/code
cd $GOPATH/src/gitlab.com/curl-paper
git clone https://gitlab.com/curl-paper/code
```

Afterwards, clone the IOTA libraries:

```
go get -u github.com/getlantern/deepcopy
go get -u github.com/iotaledger/giota
```

The latter line will emit a harmless warning (`package github.com/iotaledger/giota: no Go files in ...`). As `iotaledger` changed the implementation since we wrote our cryptanalysis code, make sure that `iotaledger` is at the right commit:

```
pushd $GOPATH/src/github.com/iotaledger/giota/
git checkout 7e48a1c9b9e904f07e1fc82815e5b302873a6dec
popd
```

Install pypy (our code hardcodes `pypy` executable name but it is likely that `pypy3` would work with small changes).

Finally, try out our attack:

```
cd $GOPATH/src/gitlab.com/curl-paper/code/valueattack
CGO_LDFLAGS_ALLOW='-msse2' go build
./valueattack
```

(The `CGO_LDFLAGS_ALLOW` environment variable enables [cgo flag whitelisting](https://github.com/golang/go/wiki/InvalidFlag) required by iotaledger at the commit we use.)
